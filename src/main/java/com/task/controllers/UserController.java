package com.task.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.task.entities.User;
import com.task.exception.DuplicateEntityException;
import com.task.exception.EntityNotFoundException;
import com.task.exception.ValidationErrorResponse;
import com.task.repositories.UserRepo;
import com.task.restviews.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UserRepo repo;

    @GetMapping
    @JsonView(View.SHORT.class)
    public List<User> all() {
        return repo.findAll();
    }

    @GetMapping("{login}")
    @JsonView(View.FULL.class)
    public User one(@PathVariable String login) {
        Optional<User> optUser = repo.findById(login);
        return optUser.orElseThrow(() -> new EntityNotFoundException(String.format("User with login:%s not found.", login)));
    }

    @DeleteMapping("{login}")
    public void delete(@PathVariable String login) {
        if (!repo.existsById(login)) {
            throw new EntityNotFoundException(String.format("User with login:%s not found.", login));
        }
        this.repo.deleteById(login);
    }

    @PostMapping()
    public ResponseEntity<ValidationErrorResponse> create(@RequestBody User user) {
        if (repo.existsById(user.getLogin())) {
            throw new DuplicateEntityException(String.format("User with login:%s already exists.", user.getLogin()));
        }
        this.repo.save(user);
        return new ResponseEntity<>(new ValidationErrorResponse(true), HttpStatus.CREATED);
    }

    @PutMapping("{login}")
    public ResponseEntity<ValidationErrorResponse> edit(@PathVariable String login, @RequestBody User user) {
        Optional<User> editedUser = repo.findById(login);

        if (!editedUser.isPresent()) throw new EntityNotFoundException(String.format("User with login:%s not found.", login));

        this.repo.save(user);
        return new ResponseEntity<>(new ValidationErrorResponse(true), HttpStatus.OK);
    }


}
