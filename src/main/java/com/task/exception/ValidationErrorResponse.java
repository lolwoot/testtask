package com.task.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidationErrorResponse {

    private Boolean success;

    private List<String> errors = new ArrayList<>();

    public ValidationErrorResponse(Boolean success) {
        this.success = success;
    }

    public void addErrorMessage(String msg) {
        this.success = false;
        this.errors.add(msg);
    }

    public Boolean getSuccess() {
        return success;
    }

    public List<String> getErrors() {
        return errors;
    }
}
