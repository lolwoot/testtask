package com.task.exception;

public class EntityNotFoundException extends EntityException {
    public EntityNotFoundException(String message) {
        super(message);
    }
}
