package com.task.exception;

public class EntityException extends RuntimeException {
    public EntityException(String message) {
        super(message);
    }
}
