package com.task.exception;

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
@ResponseBody
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({ TransactionSystemException.class })
    public ResponseEntity<ValidationErrorResponse> handleConstraintViolation(TransactionSystemException ex, WebRequest request) {

        ValidationErrorResponse errorResponse = new ValidationErrorResponse(false);

        Throwable cause = ex.getRootCause();
        if (cause instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolationEx = ((ConstraintViolationException) ex.getRootCause());
            constraintViolationEx.getConstraintViolations().stream()
                    .map(ConstraintViolation::getMessage)
                    .forEach(errorResponse::addErrorMessage);
        }

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<ValidationErrorResponse> handleSQLIntegrityConstraintViolation(DataIntegrityViolationException ex) {

        ValidationErrorResponse errorResponse = new ValidationErrorResponse(false);
        errorResponse.addErrorMessage(ex.getCause().getMessage());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DuplicateEntityException.class})
    public ResponseEntity<ValidationErrorResponse> handleEntityNotFound(DuplicateEntityException ex) {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse(false);
        errorResponse.addErrorMessage(ex.getMessage());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<ValidationErrorResponse> handleEntityNotFound(EntityNotFoundException ex) {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse(false);
        errorResponse.addErrorMessage(ex.getMessage());

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}