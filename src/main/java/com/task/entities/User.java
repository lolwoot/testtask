package com.task.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.task.restviews.View;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity
@Table(name = "app_users")
public class User {

    @JsonView({View.SHORT.class, View.FULL.class})
    @Id
    @Column(name = "login", nullable = false, unique = true)
    @NotEmpty(message = "Login must not be empty")
    private String login;

    @JsonView({View.SHORT.class, View.FULL.class})
    @Column(name = "name", nullable = false)
    @NotEmpty(message = "Name must not be empty")
    private String name;

    @JsonView({View.SHORT.class, View.FULL.class})
    @Column(name = "password", nullable = false)
    @NotEmpty(message = "Password must not be empty")
    @Pattern(regexp = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).+", message = "Password must have one capital and one digit")
    private String password;

    @JsonView(View.FULL.class)
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "user_roles",
            joinColumns = { @JoinColumn(name = "login") },
            inverseJoinColumns = { @JoinColumn(name = "id") }
    )
//    @NotNull(message = "User must have roles")
    private List<Role> role;

    public User() {
    }

    public User(String login, String name, String password, List<Role> role) {
        this.login = login;
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRole() {
        return role;
    }

    public void setRole(List<Role> role) {
        this.role = role;
    }
}
