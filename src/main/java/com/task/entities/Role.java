package com.task.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.task.restviews.View;

import javax.persistence.*;

@Entity
@Table(name = "app_roles")
public class Role {

    @JsonView({View.SHORT.class, View.FULL.class})
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @JsonView({View.SHORT.class, View.FULL.class})
    @Column(unique = true)
    private String name;

    public Role() {
    }

    public Role(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
