INSERT INTO app_users(login, name, password) VALUES
    ('alexlogin', 'Alexandr', 'Alexpassword1'),
    ('anotherlogin', 'Name', 'Password1');

INSERT INTO app_roles(id, name) VALUES
    (1, 'admin'),
    (2, 'analyst'),
    (3, 'operator');

INSERT INTO user_roles(login, id) VALUES
    ('alexlogin', 1),
    ('alexlogin', 2),
    ('alexlogin', 3),
    ('anotherlogin', 2)