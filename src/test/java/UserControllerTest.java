import com.fasterxml.jackson.databind.ObjectMapper;
import com.task.Application;
import com.task.controllers.UserController;
import com.task.entities.Role;
import com.task.entities.User;
import com.task.repositories.UserRepo;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.TransactionSystemException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.in;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;

@WebMvcTest(controllers = UserController.class)
@ContextConfiguration(classes = Application.class)
class UserTests {


    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepo repo;

    private List<User> users = new ArrayList<>();

    private static final Role ADMIN = new Role(1, "Admin");
    private static final Role ANALYST = new Role(2, "Analyst");
    private static final Role OPERATOR = new Role(3, "Operator");

    private ObjectMapper objMapper = new ObjectMapper();

    @BeforeEach
    void beforeEach() {
        User user1 = new User("Login1", "Name", "passw0rD", Arrays.asList(ANALYST, OPERATOR));
        User user2 = new User("Login2", "LongName", "helloW0rld", Arrays.asList(ADMIN));

        this.users.add(user1);
        this.users.add(user2);
    }

    @Test
    void getAllUsers() throws Exception {
        given(repo.findAll()).willReturn(users);

        this.mvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(users.size())));
    }

    @Test
    void oneUserByLogin() throws Exception {
        final String userLogin = "Login1";
        final User user = users.get(0);

        given(repo.findById(userLogin)).willReturn(Optional.of(user));

        this.mvc.perform(get("/users/{login}", userLogin))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login", is(user.getLogin())))
                .andExpect(jsonPath("$.name", is(user.getName())))
                .andExpect(jsonPath("$.password", is(user.getPassword())));

    }

    @Test
    void should404WhenFindByUserLogin() throws Exception {
        final String userLogin = "Login1";

        given(repo.findById(userLogin)).willReturn(Optional.empty());

        this.mvc.perform(get("/users/{login}", userLogin))
                .andExpect(jsonPath("$.success", is(false)))
                .andExpect(status().isNotFound());

    }

    @Test
    void shouldCreateNewUser() throws Exception {
        given(repo.save(any(User.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        User newUser = new User("NewUserLogin", "NewName", "NewPassword", Arrays.asList(ADMIN));

        this.mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objMapper.writeValueAsString(newUser)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.success", is(true)));

    }

    @Test
    void shouldDeleteUser() throws Exception {
        final String login = "userLogin";

        given(repo.existsById(login)).willReturn(true);
        doNothing().when(repo).deleteById(login);

        this.mvc.perform(delete("/users/{login}", login))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDeleteUserNotFoundThen404() throws Exception {
        final String login = "userLogin";

        given(repo.existsById(login)).willReturn(false);
        doNothing().when(repo).deleteById(login);

        this.mvc.perform(delete("/users/{login}", login))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.success", is(false)));
    }

    @Test
    void shouldReturn400WhenCreatedNewUserWithInvalidPassword() throws Exception {
        //password without digit and capital character
        final String userLogin = "TestLogin";
        User invalidUser = new User(userLogin, "TestName", "password", Arrays.asList(ADMIN));

        final TransactionSystemException exception = new TransactionSystemException("exception message");

        given(repo.findById(userLogin)).willReturn(Optional.of(invalidUser));
        given(repo.save(any(User.class))).willThrow(exception);

        this.mvc.perform(put("/users/{login}", userLogin)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objMapper.writeValueAsString(invalidUser)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.success", is(false)));
    }


}
